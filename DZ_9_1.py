import math


class Figure(object):
    def __init__(self, perimeter=None, square=None):
        self.perimeter = perimeter
        self.square = square

    def calc_perimeter(self):
        raise NotImplemented

    def calc_square(self):
        raise NotImplemented


class Triangle(Figure):
    def __init__(self, sides):
        if len(sides) != 3:
            raise Exception('Triangle takes exactly 3 sides')
        super().__init__(self)
        self.sides = sides

    def calc_perimeter(self):
        perimeter = sum(self.sides)
        return perimeter

    def calc_square(self):
        p = self.calc_perimeter() / 2
        square = math.sqrt(p * (p - self.sides[0]) * (p - self.sides[1]) * (p - self.sides[2]))
        return square


class Rectangle(Figure):
    def __init__(self, sides):
        if len(sides) != 2:
            raise Exception('Rectangle takes exactly 2 sides')
        super().__init__(self)
        self.sides = sides

    def calc_perimeter(self):
        perimeter = sum(self.sides) * 2
        return perimeter

    def calc_square(self):
        square = self.sides[0] * self.sides[1]
        return square


class Circle(Figure):
    def __init__(self, r):
        super().__init__(self)
        self.r = r

    def calc_perimeter(self):
        perimeter = 2 * math.pi * self.r
        return perimeter

    def calc_square(self):
        square = math.pi * math.pow(self.r, 2)
        return square


fig1 = Rectangle((5, 5))
print(fig1.calc_perimeter())
print(fig1.calc_square())

fig2 = Triangle((3, 4, 5))
print(fig2.calc_perimeter())
print(fig2.calc_square())

fig3 = Circle(4)
print(fig3.calc_perimeter())
print(fig3.calc_square())
