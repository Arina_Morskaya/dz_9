class User(object):
    def __init__(self):
        self._unit_name = None
        self._mac_address = None
        self._ip_address = None
        self._login = None
        self._password = None

    @property
    def data(self):
        return self._unit_name, self.mac_address, self._ip_address, self._login, self._password

    @data.setter
    def data(self, user_info):
        try:
            self._unit_name = user_info[0]
            self._mac_address = user_info[1]
            self._ip_address = user_info[2]
            self._login = user_info[3]
            self._password = user_info[4]
        except IndexError:
            pass


class Users:
    def __init__(self):
        self.users = {}

    def users_dict(self, x):
        self.users[x._unit_name] = x.__dict__
        return self.users


u = User()
u.data = ['SomeName', '50:40:5D:6E:8C:25', '192.168.1.1', 'SomeLogin', 'qwerty']
print(u._unit_name)
print(u.__dict__)

result = Users()
print(result.users_dict(u))
print(result.__dict__)

