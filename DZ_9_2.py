def singleton(cls):
    instances = {}

    def get_instance(*args):
        if cls not in instances:
            instances[cls] = cls(*args)
        return instances[cls]
    return get_instance


@singleton
class MyClass:
    def func(self, a, b):
        return a + b


m = MyClass()
print(id(m))
print('Function result -', m.func(5, 25))
print(id(m.func(5, 25)), end='\n\n')

n = MyClass()
print(id(n))
print('Function result -', n.func(5, 2))
print(id(n.func(5, 2)), end='\n\n')

k = MyClass()
print(id(k))
print('Function result -', k.func(5, 2))
print(id(k.func(5, 2)))
